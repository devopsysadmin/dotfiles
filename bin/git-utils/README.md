# Git-utils

## Previous

declare in your PATH this directory. Ex:
```bash
export PATH=$PATH:$HOME/.local/git-utils
```


## Cheat sheet
| cmd | name | descrp |
|-----|------|--------|
| gbd | git-branch-delete | Deletes local AND remote branch |
| gcd | git-commit-dirty | git add -A . + git commit -m "Dirty commit" + git push |
| gci | git-commit-issue | Prepends a issue given the branch name |
| gco | git checkout | |
| | git-clean-conflict | Deletes files created by a merge conflict (\*ORIG\*, \*DEST\*) |
|| git-ignore | Adds a new line to .gitignore (doesn't if already exists) |
|| git-prune | Deletes local branches not found in remote |
|| git-retag | Deletes local and remote tag and creates a new one based on current HEAD |
| gl | git pull ||
| gl1 | git log --oneline ||
| gls | git pull --set-upstream {current_branch} ||
| gpf | git push --force ||
| gps | git push --set-upstream {current_branch} ||
| gpt | git push --tags ||
| gri | git rebase --interactive ||
| gtd | git-tag-delete | Deletes local and remote tag |
